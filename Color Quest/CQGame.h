//
//  CQGame.h
//  Color Quest
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CQBlock.h"

@interface CQGame : NSObject

@property (strong, nonatomic) NSMutableArray *board;
@property (nonatomic) NSInteger height;
@property (nonatomic) NSInteger width;
@property (nonatomic, readonly) NSInteger gameStatus;
@property (strong, nonatomic) NSMutableArray *matchingBlocks;
@property (nonatomic) int difficulty;


-(instancetype) initWithDifficulty:(int)difficulty;
-(BOOL) isMatchOfThisBlock:(CQBlock *) thisBlock andThatBlock:(CQBlock *)thatBlock;
-(NSString *) getDifficultyName;
+(NSString *) getDifficultyName:(int) difficulty;

typedef enum
{
    STATE_NIL,
    STATE_IN_PROGRESS,
    STATE_CORRECT,
    STATE_RESET,
    STATE_WRONG,
} GAME_STATE;

typedef enum
{
    DIFFICULTY_EASY = 3,
    DIFFICULTY_MEDIUM,
    DIFFICULTY_HARD,
    DIFFICULTY_VERY_HARD,
    DIFFICULTY_IMPOSSIBLE,
    DIFFICULTY_END
} DIFFICULTY_LEVEL;
@end

