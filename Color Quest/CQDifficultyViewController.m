//
//  CQDifficultyViewController.m
//  Color Quest
//
//  Created by Jeremy Levine on 2/8/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CQDifficultyViewController.h"

@interface CQDifficultyViewController ()

@end

@implementation CQDifficultyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:IMAGE_NAME_BACKGROUND_MENU]];
    [self.view addSubview:background];
    
    UIButton *backButton = [CQSharedManager makeBackButton];
    [backButton addTarget:self action:@selector(clickBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    UILabel *headingLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-HEADING_WIDTH/2, TOP_GAP_LABEL, HEADING_WIDTH, HEADING_HEIGHT)];
    [headingLabel setTextColor:[CQSharedManager sharedManager].headingColor];
    headingLabel.text = @"Choose difficulty";
    headingLabel.textAlignment = NSTextAlignmentCenter;
    [headingLabel setFont:[UIFont systemFontOfSize:HEADING_FONT_SIZE]];
    [self.view addSubview:headingLabel];

    
    for(int i = DIFFICULTY_EASY; i < DIFFICULTY_END; i++) {
        UIButton *difficultyButton = [CQSharedManager makeMenuButtonWithX: self.view.frame.size.width/2-MENU_WIDTH/2 AndY: TOP_GAP_INFO+(MENU_HEIGHT+BUTTON_GAP)*(i-DIFFICULTY_EASY)];
        [difficultyButton setTitle:[CQSharedManager getDifficultyName:i] forState:UIControlStateNormal];
        difficultyButton.tag = i;
        [difficultyButton addTarget:self action:@selector(clickDifficultyButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:difficultyButton];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) clickDifficultyButton:(UIButton *) sender
{
    CQMedalViewController *medalViewController = [[CQMedalViewController alloc] init];
    medalViewController.difficulty = sender.tag;
    [self.navigationController pushViewController:medalViewController animated:YES];
    
    /*CQGameViewController *gameViewController = [[CQGameViewController alloc] init];
    gameViewController.difficulty = sender.tag;
    [self.navigationController pushViewController:gameViewController animated:YES];*/
}

-(void) clickBackButton:(UIButton*) sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
