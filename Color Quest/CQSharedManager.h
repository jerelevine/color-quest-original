//
//  CQSharedManager.h
//  Color Quest
//
//  Created by Jeremy Levine on 2/10/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CQSharedManager : NSObject

extern int const BLOCK_MIN;
extern int const BLOCK_MAX;
extern double const TOP_GAP_BOARD;
extern double const TOP_GAP_LABEL;
extern double const TOP_GAP_INFO;
extern double const SIDE_GAP;
extern double const BUTTON_GAP;


extern double const BLOCK_BORDER_WIDTH_NORMAL;
extern double const BLOCK_BORDER_WIDTH_HIGHLIGHTED;
extern double const BUTTON_BORDER_WIDTH;
extern double const CORNER_RADIUS;

extern int const TIMER_START;
extern int const TIMER_MULTIPLE;
extern double const DISPLAY_TIME_WRONG;
extern double const DISPLAY_TIME_RESET;
extern double const DISPLAY_TIME_CORRECT;

extern double const MENU_WIDTH;
extern double const MENU_HEIGHT;

extern double const TITLE_WIDTH;
extern double const TITLE_HEIGHT;

extern double const LABEL_WIDTH;
extern double const LABEL_HEIGHT;

extern double const HEADING_WIDTH;
extern double const HEADING_HEIGHT;

extern double const AUXILIARY_WIDTH;
extern double const AUXILIARY_HEIGHT;


extern double const TITLE_FONT_SIZE;
extern double const HEADING_FONT_SIZE;
extern double const MENU_FONT_SIZE;
extern double const LABEL_FONT_SIZE;


extern NSString * const APP_TITLE;

extern NSString * const IMAGE_NAME_IN_PROGRESS;
extern NSString * const IMAGE_NAME_CORRECT;
extern NSString * const IMAGE_NAME_WRONG;

extern NSString *const IMAGE_NAME_BACKGROUND_GAME;
extern NSString *const IMAGE_NAME_BACKGROUND_MENU;

@property (strong, nonatomic, readonly) UIColor *headingColor;

@property (strong, nonatomic, readonly) UIColor *menuButtonColorNormal;
@property (strong, nonatomic, readonly) UIColor *menuButtonColorHighlighted;
@property (strong, nonatomic, readonly) UIColor *menuButtonBackgroundColor;


@property (strong, nonatomic, readonly) UIColor *buttonBorderColorNormal;
@property (strong, nonatomic, readonly) UIColor *buttonBorderColorHighlighted;
@property (strong, nonatomic, readonly) UIColor *buttonBorderColorHighlightedAlternative;

@property (strong, nonatomic, readonly) UIColor *infoColor;

@property (strong, nonatomic, readonly) UIColor *goldColor;
@property (strong, nonatomic, readonly) UIColor *silverColor;
@property (strong, nonatomic, readonly) UIColor *bronzeColor;

@property (strong, nonatomic, readonly) NSMutableDictionary *medalDictionary;


+(instancetype) sharedManager;
+(UIButton *) makeBackButton;
+(UIButton *) makeMenuButtonWithX:(double) x AndY:(double) y;
+(NSString *) getDifficultyName:(int) difficulty;

@end
