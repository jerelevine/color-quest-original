//
//  CQMedalViewController.m
//  Color Quest
//
//  Created by Jeremy Levine on 2/11/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CQMedalViewController.h"

@interface CQMedalViewController ()

@end

@implementation CQMedalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:IMAGE_NAME_BACKGROUND_MENU]];
    [self.view addSubview:background];
    
    UIButton *backButton = [CQSharedManager makeBackButton];
    [backButton addTarget:self action:@selector(clickBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    UILabel *headingLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-HEADING_WIDTH/2, TOP_GAP_LABEL, HEADING_WIDTH, HEADING_HEIGHT)];
    [headingLabel setTextColor:[CQSharedManager sharedManager].headingColor];
    headingLabel.text = [CQSharedManager getDifficultyName:self.difficulty];
    headingLabel.textAlignment = NSTextAlignmentCenter;
    [headingLabel setFont:[UIFont systemFontOfSize:HEADING_FONT_SIZE]];
    [self.view addSubview:headingLabel];
    
    UIButton *menuButton = [CQSharedManager makeMenuButtonWithX: self.view.frame.size.width/2-MENU_WIDTH/2 AndY: TOP_GAP_INFO];
    [menuButton setTitle:@"Continue" forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(clickMenuButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:menuButton];
    
    UILabel *bronzeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-HEADING_WIDTH/2, TOP_GAP_INFO +(MENU_HEIGHT+BUTTON_GAP)*1, HEADING_WIDTH, MENU_HEIGHT)];
    [bronzeLabel setTextColor:[CQSharedManager sharedManager].infoColor];
    bronzeLabel.text = @"Bronze";
    bronzeLabel.textAlignment = NSTextAlignmentCenter;
    [bronzeLabel setFont:[UIFont systemFontOfSize:HEADING_FONT_SIZE]];
    [self.view addSubview:bronzeLabel];
    
    UILabel *silverLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-HEADING_WIDTH/2, TOP_GAP_INFO +(MENU_HEIGHT+BUTTON_GAP)*2, HEADING_WIDTH, MENU_HEIGHT)];
    [silverLabel setTextColor:[CQSharedManager sharedManager].infoColor];
    silverLabel.text = @"Silver";
    silverLabel.textAlignment = NSTextAlignmentCenter;
    [silverLabel setFont:[UIFont systemFontOfSize:HEADING_FONT_SIZE]];
    [self.view addSubview:silverLabel];
    
    UILabel *goldLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-HEADING_WIDTH/2, TOP_GAP_INFO +(MENU_HEIGHT+BUTTON_GAP)*3, HEADING_WIDTH, MENU_HEIGHT)];
    [goldLabel setTextColor:[CQSharedManager sharedManager].infoColor];
    goldLabel.text = @"Gold";
    goldLabel.textAlignment = NSTextAlignmentCenter;
    [goldLabel setFont:[UIFont systemFontOfSize:HEADING_FONT_SIZE]];
    [self.view addSubview:goldLabel];
    
    
    
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) clickMenuButton:(UIButton *) sender
{
    CQGameViewController *gameViewController = [[CQGameViewController alloc] init];
    gameViewController.difficulty = self.difficulty;
    [self.navigationController pushViewController:gameViewController animated:YES];
}

-(void) clickBackButton:(UIButton*) sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
