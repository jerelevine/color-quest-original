//
//  CQSharedManager.m
//  Color Quest
//
//  Created by Jeremy Levine on 2/10/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CQSharedManager.h"
#import "CQGame.h"

@implementation CQSharedManager

int const BLOCK_MAX = 40;
int const BLOCK_MIN = 32;

double const TOP_GAP_BOARD = 160;
double const TOP_GAP_LABEL = 35;
double const TOP_GAP_INFO = 80;
double const SIDE_GAP = 16;
double const BUTTON_GAP = 20;


double const BLOCK_BORDER_WIDTH_NORMAL = 2.0f;
double const BLOCK_BORDER_WIDTH_HIGHLIGHTED = 4.0f;
double const BUTTON_BORDER_WIDTH = 1.0f;
double const CORNER_RADIUS = 3.0f;

double const MENU_WIDTH = 150.0f;
double const MENU_HEIGHT = 40.0f;

double const TITLE_WIDTH = 300.0f;
double const TITLE_HEIGHT = 100.0f;

double const HEADING_WIDTH = 300.0f;
double const HEADING_HEIGHT = 30.0f;

double const TITLE_FONT_SIZE = 40.0f;
double const HEADING_FONT_SIZE = 25.0f;
double const MENU_FONT_SIZE = 18.0f;
double const LABEL_FONT_SIZE = 45.0f;

double const AUXILIARY_WIDTH = 30;
double const AUXILIARY_HEIGHT = 30;

double const LABEL_WIDTH = 80;
double const LABEL_HEIGHT = 50;


int const TIMER_START = 90;
int const TIMER_MULTIPLE = 15;

double const DISPLAY_TIME_WRONG = 0.75;
double const DISPLAY_TIME_RESET = 0.75;
double const DISPLAY_TIME_CORRECT = 0.25;

NSString * const IMAGE_NAME_IN_PROGRESS = @"CQ_cycle.png";
NSString * const IMAGE_NAME_CORRECT = @"CQ_checkmark";
NSString * const IMAGE_NAME_WRONG = @"CQ_crossmark.png";

NSString * const IMAGE_NAME_BACKGROUND_MENU = @"CQ_background_menu.png";
NSString * const IMAGE_NAME_BACKGROUND_GAME = @"CQ_background_game.png";


NSString * const APP_TITLE = @"Color Quest";

+ (instancetype)sharedManager {
    static CQSharedManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _menuButtonColorNormal = [UIColor colorWithRed:0.15f green:0.15f blue:0.85f alpha:0.85f];
        _menuButtonColorHighlighted = [UIColor colorWithRed:0.15f green:0.15f blue:0.85f alpha:0.35f];
        _menuButtonBackgroundColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.75f];
        _headingColor = [UIColor colorWithWhite:0.25f alpha:0.75f];
        _buttonBorderColorNormal = [UIColor blackColor];
        _buttonBorderColorHighlighted = [UIColor redColor];
        _buttonBorderColorHighlightedAlternative = [UIColor cyanColor];
        
        _infoColor = [UIColor colorWithWhite:0.90f alpha:0.90f];

        
        _goldColor = [UIColor colorWithRed:255.0f/255.0f green:215.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
        _silverColor = [UIColor colorWithRed:192.0f/255.0f green:192.0f/255.0f blue:192.0f/255.0f alpha:1.0f];
        _bronzeColor = [UIColor colorWithRed:205.0f/255.0f green:127.0f/255.0f blue:50.0f/255.0f alpha:1.0f];
        
        
        _medalDictionary = [[NSMutableDictionary alloc] init];
        for(int i = DIFFICULTY_EASY; i < DIFFICULTY_END; i++) {
            NSString *key = [NSString stringWithFormat:@"%i",i];
            int gold = floor(500.0f/pow(i,2));
            int silver = floor(300.0f/pow(i,2));
            int bronze = floor(100.0f/pow(i,2));
            NSArray *value = @[[NSNumber numberWithInt:bronze], [NSNumber numberWithInt:silver], [NSNumber numberWithInt:gold]];
            [_medalDictionary setValue: value forKey: key];
        }
    }
    return self;
}

+ (UIButton *) makeBackButton {
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(SIDE_GAP, TOP_GAP_LABEL, AUXILIARY_WIDTH, AUXILIARY_HEIGHT)];
    [backButton setTitle: @"←" forState:UIControlStateNormal];
    [backButton setTitleColor:[CQSharedManager sharedManager].menuButtonColorNormal forState:UIControlStateNormal];
    [backButton setTitleColor:[CQSharedManager sharedManager].menuButtonColorHighlighted forState:UIControlStateHighlighted];
    
    backButton.backgroundColor = [CQSharedManager sharedManager].menuButtonBackgroundColor;
    backButton.layer.borderColor = ([CQSharedManager sharedManager].menuButtonColorNormal).CGColor;
    backButton.layer.borderWidth = BUTTON_BORDER_WIDTH;
    backButton.layer.cornerRadius = CORNER_RADIUS;
    [backButton.titleLabel setFont:[UIFont systemFontOfSize:MENU_FONT_SIZE]];
    backButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    backButton.clipsToBounds = YES;
    return backButton;
}

+ (UIButton *) makeMenuButtonWithX:(double) x AndY:(double) y {
    UIButton *menuButton = [[UIButton alloc] initWithFrame: CGRectMake(x, y, MENU_WIDTH, MENU_HEIGHT)];
    [menuButton setTitleColor:[CQSharedManager sharedManager].menuButtonColorNormal forState:UIControlStateNormal];
    [menuButton setTitleColor:[CQSharedManager sharedManager].menuButtonColorHighlighted forState:UIControlStateHighlighted];
    menuButton.backgroundColor = [CQSharedManager sharedManager].menuButtonBackgroundColor;
    menuButton.layer.borderColor = ([CQSharedManager sharedManager].menuButtonColorNormal).CGColor;
    menuButton.layer.borderWidth = BUTTON_BORDER_WIDTH;
    menuButton.layer.cornerRadius = CORNER_RADIUS;
    menuButton.clipsToBounds = YES;
    return menuButton;
}


+(NSString *) getDifficultyName:(int) difficulty {
    switch (difficulty) {
        case DIFFICULTY_EASY:
            return @"Easy";
        case DIFFICULTY_MEDIUM:
            return @"Medium";
        case DIFFICULTY_HARD:
            return @"Hard";
        case DIFFICULTY_VERY_HARD:
            return @"Very Hard";
        case DIFFICULTY_IMPOSSIBLE:
            return @"Impossible";
        default:
            return @"error";
    }
}


@end
