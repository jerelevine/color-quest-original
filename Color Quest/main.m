//
//  main.m
//  Color Quest
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CQAppDelegate class]));
    }
}
