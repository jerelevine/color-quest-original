//
//  CQAppDelegate.h
//  Color Quest
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "CQMainViewController.h"

@interface CQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
