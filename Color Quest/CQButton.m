//
//  CQButton.m
//  Color Quest
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CQButton.h"

@implementation CQButton



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    return self;
}

- (instancetype) initWithLocation:(CGPoint)point andBlock:(CQBlock *)block andSize:(NSInteger)blockSize AndIsLabelDisplayed:(BOOL)isLabelDisplayed {
    self = [super initWithFrame:CGRectMake(point.x*blockSize, point.y*blockSize, blockSize, blockSize)];
    if (self) {
        self.block = block;
        [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        [self.titleLabel setFont:[UIFont systemFontOfSize:ceil(blockSize/5.0f)]];
        [self setTitleColor: [UIColor blackColor] forState:UIControlStateNormal];
        CGFloat red, green, blue;
        [self.block.color getRed:&red green:&green blue:&blue alpha:nil];
        if((red + green + blue/2.0f) >= 1)
            [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        else
            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self setIsLabelDisplayed:isLabelDisplayed];
        self.backgroundColor = block.color;
        self.layer.borderColor = ([CQSharedManager sharedManager].buttonBorderColorNormal).CGColor;
        self.layer.borderWidth = BLOCK_BORDER_WIDTH_NORMAL;
        self.layer.cornerRadius = CORNER_RADIUS;
        self.clipsToBounds = YES;
        self.point = point;
    }
    return self;
}

-(void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    if(enabled) {
        self.layer.borderColor = ([CQSharedManager sharedManager].buttonBorderColorNormal).CGColor;
        self.layer.borderWidth = BLOCK_BORDER_WIDTH_NORMAL;
    }
    else {
        CGFloat red, green, blue;
        [self.block.color getRed:&red green:&green blue:&blue alpha:nil];
        if(red > 0.5 && red > green && red >= blue)
            self.layer.borderColor = ([CQSharedManager sharedManager].buttonBorderColorHighlightedAlternative).CGColor;
        else
            self.layer.borderColor = ([CQSharedManager sharedManager].buttonBorderColorHighlighted).CGColor;
        self.layer.borderWidth = BLOCK_BORDER_WIDTH_HIGHLIGHTED;
    }

}

-(void) showMatching {
    if (self.block.hasMatch) {
        self.enabled = NO;
    }
    else {
        self.enabled = YES;
    }
}

-(void) setIsLabelDisplayed:(BOOL)isLabelDisplayed {
    _isLabelDisplayed = isLabelDisplayed;
    if(isLabelDisplayed)
        [self setTitle: self.block.label forState:UIControlStateNormal];
    else
        [self setTitle: nil forState:UIControlStateNormal];

}



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
