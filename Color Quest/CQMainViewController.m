//
//  CQGameViewController.m
//  Color Quest
//
//  Created by Jeremy Levine on 2/7/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CQMainViewController.h"

@interface CQMainViewController ()

@end

@implementation CQMainViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:IMAGE_NAME_BACKGROUND_MENU]];
    [self.view addSubview:background];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-TITLE_WIDTH/2, TOP_GAP_LABEL, TITLE_WIDTH, TITLE_HEIGHT)];
    
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleLabel setFont:[UIFont systemFontOfSize:TITLE_FONT_SIZE]];
    [titleLabel setText:APP_TITLE];
    [titleLabel setTextColor:[CQSharedManager sharedManager].headingColor];
    [self.view addSubview:titleLabel];
    
    self.startButton = [CQSharedManager makeMenuButtonWithX:self.view.frame.size.width/2-MENU_WIDTH/2 AndY:TOP_GAP_INFO+MENU_HEIGHT];
    [self.startButton setTitle:@"Start Game" forState:UIControlStateNormal];
    [self.startButton addTarget:self action:@selector(clickStart:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.startButton];
    
    self.optionButton = [CQSharedManager makeMenuButtonWithX: self.view.frame.size.width/2-MENU_WIDTH/2 AndY: TOP_GAP_INFO+MENU_HEIGHT*2+BUTTON_GAP];
    [self.optionButton setTitle:@"Options" forState:UIControlStateNormal];
    [self.optionButton addTarget:self action:@selector(clickOption:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.optionButton];


    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) clickStart:(UIButton *)sender {
    CQDifficultyViewController *gameViewController = [[CQDifficultyViewController alloc] init];
    [self.navigationController pushViewController:gameViewController animated:YES];
}

- (void) clickOption:(UIButton *)sender {
    CQOptionViewController *optionViewController = [[CQOptionViewController alloc] init];
    [self.navigationController pushViewController:optionViewController animated:YES];
}

@end
