//
//  CQButton.h
//  Color Quest
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CQBlock.h"
#import "CQSharedManager.h"

@interface CQButton : UIButton

@property (nonatomic) CGPoint point;
@property (weak, nonatomic) CQBlock * block;
@property (nonatomic) BOOL isLabelDisplayed;

- (instancetype) initWithLocation:(CGPoint)point andBlock:(CQBlock *) block andSize:(NSInteger)blockSize AndIsLabelDisplayed:(BOOL)isLabelDisplayed;
- (void) showMatching;

@end
