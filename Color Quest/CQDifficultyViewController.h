//
//  CQDifficultyViewController.h
//  Color Quest
//
//  Created by Jeremy Levine on 2/8/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CQGame.h"
#import "CQMedalViewController.h"
#import "CQGameViewController.h"
#import "CQSharedManager.h"


@interface CQDifficultyViewController : UIViewController

@end
