//
//  CQViewController.h
//  Color Quest
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "CQButton.h"
#import "CQGame.h"
#import "CQOptionViewController.h"
#import "CQSharedManager.h"



@interface CQGameViewController : UIViewController<ADBannerViewDelegate>
@property (strong, nonatomic) NSMutableArray *buttonArray;
@property (strong, nonatomic) CQGame *game;
@property (strong, nonatomic) UISegmentedControl *clickTypeButton;
@property (strong, nonatomic) UIButton *mainButton;
@property (weak, nonatomic) NSTimer *timer;
@property (strong, nonatomic) UILabel *timerLabel;
@property (strong, nonatomic) UILabel *puzzleLabel;
@property (nonatomic) int timeCount;
@property (nonatomic) int puzzleCount;
@property (nonatomic) BOOL isLabelDisplayed;

@property (strong, nonatomic) CQButton *firstChoice;
@property (strong, nonatomic) CQButton *secondChoice;
@property (nonatomic) int gameState;

@property (strong, nonatomic) UIView *buttonSubview;
@property (nonatomic) int difficulty;

@property (nonatomic) BOOL isPaused;




@end
