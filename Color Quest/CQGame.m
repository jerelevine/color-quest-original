//
//  CQGame.m
//  Color Quest
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CQGame.h"

@implementation CQGame

-(instancetype) init {
    self = [super init];
    return self;
}

-(instancetype) initWithDifficulty:(int)difficulty
{
    self = [super init];
    if(self) {
        _gameStatus = STATE_IN_PROGRESS;
        self.difficulty = difficulty;
        self.width = difficulty;
        self.height = difficulty;
        
        
        NSMutableSet *colorSet = [[NSMutableSet alloc] init];
        NSMutableSet *labelSet = [[NSMutableSet alloc] init];

        
        while(colorSet.count < ((self.width * self.height) - 1)) {
            double ratio = (ceil(pow(self.width*self.height-1, 1.0f/3.0f)));
            double red = (arc4random_uniform((int32_t)ratio)/(ratio - 1));
            double green = (arc4random_uniform((int32_t)ratio)/(ratio - 1));
            double blue = (arc4random_uniform((int32_t)ratio)/(ratio - 1));
            UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
            
            
            
            [colorSet addObject:color];
        }

        while(labelSet.count < ((self.width * self.height) - 1)) {
            if((self.width * self.height)>26*26) [NSException raise:@"Invalid Block amount value" format:@"Block amount of %i is invalid", (int) (self.width * self.height)];
            int num1 = (int)(arc4random_uniform(26))+65;
            int num2 = (int)(arc4random_uniform(26))+65;
            NSString *label;
            if(((self.width * self.height) - 1) <= 26)
                label = [NSString stringWithFormat:@"%c", num1];
            else
                label = [NSString stringWithFormat:@"%c%c", num1, num2];
            [labelSet addObject:label];
        }
        
        
        NSMutableArray *colorArray = [NSMutableArray arrayWithArray:[colorSet allObjects]];
        
        NSMutableArray *labelArray = [NSMutableArray arrayWithArray:[labelSet allObjects]];
        NSMutableArray *blockArray = [[NSMutableArray alloc] init];
        for(int i = 0; i < colorArray.count; i++) {
            [blockArray addObject:[[CQBlock alloc] initWithColor:colorArray[i] AndLabel:labelArray[i]]];
        }
        int repeatIndex = arc4random_uniform((int32_t)blockArray.count);
        CQBlock *repeat = [blockArray[repeatIndex] copy];
        int insertIndex = arc4random_uniform((int32_t)blockArray.count);
        [blockArray[repeatIndex] setHasMatch:YES];
        [repeat setHasMatch: YES];
        [blockArray insertObject:repeat atIndex:insertIndex];

        
        self.board = [NSMutableArray arrayWithCapacity:self.width];
        for(int i = 0; i < self.width; i++) {
            self.board[i] = [NSMutableArray arrayWithCapacity:self.height];
            for(int j = 0; j < self.height; j++) {
                int loc = arc4random_uniform((int32_t)blockArray.count);
                CQBlock *block = blockArray[loc];
                [blockArray removeObjectAtIndex:loc];
                self.board[i][j] = block;
            }
        }
    }
    return self;
}

-(BOOL) isMatchOfThisBlock:(CQBlock *)thisBlock andThatBlock:(CQBlock *)thatBlock
{
    return [thisBlock.color isEqual:thatBlock.color];
}


@end

