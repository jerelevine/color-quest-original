//
//  CQMedalViewController.h
//  Color Quest
//
//  Created by Jeremy Levine on 2/11/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CQSharedManager.h"
#import "CQGameViewController.h"

@interface CQMedalViewController : UIViewController

@property (nonatomic) int difficulty;

@end
