//
//  CQBlock.h
//  Color Quest
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CQBlock : NSObject

@property (strong, nonatomic) UIColor * color;
@property (strong, nonatomic) NSString * label;
@property (nonatomic) CGPoint indices;
@property (nonatomic) BOOL hasMatch;

-(instancetype) initWithColor:(UIColor *)color AndLabel:(NSString *)label;

@end
