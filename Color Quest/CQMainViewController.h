//
//  CQGameViewController.h
//  Color Quest
//
//  Created by Jeremy Levine on 2/7/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CQDifficultyViewController.h"
#import "CQOptionViewController.h"

@interface CQMainViewController : UIViewController

@property (strong, nonatomic) UIButton *startButton;
@property (strong, nonatomic) UIButton *optionButton;




@end
