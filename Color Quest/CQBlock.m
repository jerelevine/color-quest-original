//
//  CQBlock.m
//  Color Quest
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CQBlock.h"

@implementation CQBlock

-(instancetype) init {
    self = [super init];
    return self;
}

-(instancetype) initWithColor:(UIColor *)color AndLabel:(NSString *)label{
    self = [super init];
    if(self) {
        self.color = color;
        self.label = label;
        self.hasMatch = NO;
    }
    return self;
}

-(instancetype) copy {
    return [[CQBlock alloc] initWithColor:self.color AndLabel:self.label];
}

@end

