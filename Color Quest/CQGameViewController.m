//
//  CQViewController.m
//  Color Quest
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CQGameViewController.h"

@interface CQGameViewController () {
IBOutlet ADBannerView *topAdBanner;
}

@end

@implementation CQGameViewController

-(void)viewDidDisappear:(BOOL)animated {
    if(self.isPaused) [self.timer invalidate];
        
}

-(void) viewDidAppear:(BOOL)animated {
    if(self.isPaused) [self onTick:self.timer];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setup];
}

#pragma mark - iAdBanner Delegates

-(void)bannerView:(ADBannerView *)banner
didFailToReceiveAdWithError:(NSError *)error{
    NSLog(@"Error in Loading Banner!");
}

-(void)bannerViewDidLoadAd:(ADBannerView *)banner{
    NSLog(@"iAd banner Loaded Successfully!");
}
-(void)bannerViewWillLoadAd:(ADBannerView *)banner{
    NSLog(@"iAd Banner will load!");
}
-(void)bannerViewActionDidFinish:(ADBannerView *)banner{
    NSLog(@"iAd Banner did finish");
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setup
{
    

    self.isPaused = NO;
    
    self.isLabelDisplayed = NO;
    self.timeCount = TIMER_MULTIPLE * (self.difficulty + 1);
    
   
    self.buttonArray = [[NSMutableArray alloc] init];
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:IMAGE_NAME_BACKGROUND_GAME]];
    [self.view addSubview:background];
    
    self.firstChoice = nil;
    self.secondChoice = nil;
    self.puzzleCount = 0;
    
    UILabel *headingLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-HEADING_WIDTH/2, TOP_GAP_LABEL, HEADING_WIDTH, HEADING_HEIGHT)];
    [headingLabel setTextColor:[CQSharedManager sharedManager].headingColor];
    headingLabel.text = APP_TITLE;
    headingLabel.textAlignment = NSTextAlignmentCenter;
    [headingLabel setFont:[UIFont systemFontOfSize:HEADING_FONT_SIZE]];
    [self.view addSubview:headingLabel];
    
    UIButton *backButton = [CQSharedManager makeBackButton];
    [backButton addTarget:self action:@selector(clickBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    UIButton *optionButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-SIDE_GAP-AUXILIARY_WIDTH, TOP_GAP_LABEL, AUXILIARY_WIDTH, AUXILIARY_HEIGHT)];
    [optionButton setTitle: @"i" forState:UIControlStateNormal];
    [optionButton setTitleColor:[CQSharedManager sharedManager].menuButtonColorNormal forState:UIControlStateNormal];
    [optionButton setTitleColor:[CQSharedManager sharedManager].menuButtonColorHighlighted forState:UIControlStateHighlighted];
    optionButton.backgroundColor = [CQSharedManager sharedManager].menuButtonBackgroundColor;
    optionButton.layer.borderColor = ([CQSharedManager sharedManager].menuButtonColorNormal).CGColor;
    optionButton.layer.borderWidth = BUTTON_BORDER_WIDTH;
    optionButton.layer.cornerRadius = CORNER_RADIUS;
    [optionButton.titleLabel setFont:[UIFont systemFontOfSize:MENU_FONT_SIZE]];
    optionButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    optionButton.clipsToBounds = YES;
    [optionButton addTarget:self action:@selector(clickOptionButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:optionButton];


    
    self.puzzleLabel = [[UILabel alloc] initWithFrame:CGRectMake(SIDE_GAP, TOP_GAP_INFO, LABEL_WIDTH, LABEL_HEIGHT)];
    self.puzzleLabel.text = [self labelString:self.puzzleCount];
    self.puzzleLabel.textAlignment = NSTextAlignmentCenter;
    self.puzzleLabel.layer.backgroundColor = ([CQSharedManager sharedManager].headingColor).CGColor;
    self.puzzleLabel.textColor = [UIColor whiteColor];
    [self.puzzleLabel setFont:[UIFont systemFontOfSize:LABEL_FONT_SIZE]];
    self.puzzleLabel.layer.cornerRadius = CORNER_RADIUS;
    
    [self.view addSubview:self.puzzleLabel];

    
    self.timerLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-SIDE_GAP-LABEL_WIDTH, TOP_GAP_INFO, LABEL_WIDTH, LABEL_HEIGHT)];
    self.timerLabel.text = [self labelString:self.timeCount];
    self.timerLabel.textAlignment = NSTextAlignmentCenter;
    self.timerLabel.layer.backgroundColor = ([CQSharedManager sharedManager].headingColor).CGColor;
    self.timerLabel.textColor = [UIColor whiteColor];
    [self.timerLabel setFont:[UIFont systemFontOfSize:LABEL_FONT_SIZE]];
    self.timerLabel.layer.cornerRadius = CORNER_RADIUS;

    [self.view addSubview:self.timerLabel];
    
    self.mainButton = [[UIButton alloc] init];
    self.mainButton.frame = CGRectMake(self.view.frame.size.width/2-50/2, TOP_GAP_INFO, 50, 50);
    self.mainButton.backgroundColor = [UIColor lightGrayColor];
    self.mainButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.mainButton.layer.borderWidth = BUTTON_BORDER_WIDTH;
    self.mainButton.layer.cornerRadius = CORNER_RADIUS;
    [self.mainButton.titleLabel setFont:[UIFont systemFontOfSize:50]];
    self.mainButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.mainButton addTarget:self action:@selector(clickMainButton:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.mainButton];
    
    self.gameState = STATE_IN_PROGRESS;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Get Ready!" message:[NSString stringWithFormat:@"Click Ok to start"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

-(void) newGame {
    if(!self.timer) self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(onTick:) userInfo:nil repeats:NO];

    self.gameState = STATE_IN_PROGRESS;
    
    [self.buttonArray removeAllObjects];
    self.game = [[CQGame alloc] initWithDifficulty:self.difficulty];
    int blockSize = fmin(320.0f/self.difficulty, BLOCK_MAX);//(self.difficulty >= BLOCK_MAX ? BLOCK_SMALL: BLOCK_LARGE);
    if(blockSize < BLOCK_MIN) [NSException raise:@"Invalid Block size value" format:@"Block size of %d is invalid", blockSize];
    
    [self.buttonSubview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.buttonSubview removeFromSuperview];
    
    self.buttonSubview = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2.0f-(self.game.width*blockSize)/2.0f), TOP_GAP_BOARD, self.game.width * blockSize, self.game.height * blockSize)];
    
    for(int i = 0; i < self.game.height; i++) {
        for (int j = 0; j < self.game.width; j++) {
            CQButton *button = [[CQButton alloc] initWithLocation: CGPointMake(j,i) andBlock:self.game.board[j][i] andSize:blockSize AndIsLabelDisplayed:self.isLabelDisplayed];
            [self.buttonArray addObject:button];
            [self.buttonSubview addSubview:button];
            [button addTarget:self action:@selector(clickBlockButton:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    [self.view addSubview:self.buttonSubview];
}

-(void) onTick:(NSTimer *)timer {
    self.timeCount--;
    self.timerLabel.text = [self labelString:self.timeCount];
    if(self.timeCount <= 0) {
        [self gameOver];
    }
    else {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(onTick:) userInfo:nil repeats:NO];
    }
}

-(NSString *) labelString:(int) num {
    if(num < 10) {
        return [NSString stringWithFormat:@"00%i",num];
    }
    else if(num < 100) {
        return [NSString stringWithFormat:@"0%i",num];
    }
    else if(num < 1000) {
        return [NSString stringWithFormat:@"%i",num];
    }
    else
        return @"---";
}


-(void) clickBlockButton:(CQButton *)sender
{
    if(self.gameState == STATE_RESET || self.gameState == STATE_WRONG) return;
    
    UIColor *color = sender.backgroundColor;
    CGFloat red, green, blue, alpha;
    [color getRed: &red green: &green blue: &blue alpha: &alpha];
    double x = sqrt(pow(red, 2) + pow(green, 2) + pow(blue,2));
    NSLog(@"Color: (%f, %f, %f) %@, %f", red, green, blue, sender.titleLabel.text, x);
    
    if(!self.firstChoice) {
        self.firstChoice = sender;
        sender.enabled = NO;
    }
    else if(self.firstChoice && [[self.firstChoice.block color] isEqual:[sender.block color]]) {
        self.secondChoice = sender;
        [self gameCorrect];
    }
    else if(self.gameState != STATE_CORRECT){
        self.secondChoice = sender;
        [self gameWrong];
    }
}

-(void) clickMainButton:(UIButton *)sender {
    if(self.gameState == STATE_IN_PROGRESS) {
        [self gameReset];
    }
}

-(void) gameWrong {
    self.gameState = STATE_WRONG;
    self.firstChoice.enabled = NO;
    self.secondChoice.enabled = NO;
    [NSTimer scheduledTimerWithTimeInterval:DISPLAY_TIME_WRONG target:self selector:@selector(setGameInProgress) userInfo:nil repeats:NO];
}

-(void) gameReset {
    for(CQButton *b in self.buttonArray) {
        [b showMatching];
    }
    self.gameState = STATE_RESET;
    self.firstChoice = nil;
    self.secondChoice = nil;
    [NSTimer scheduledTimerWithTimeInterval:DISPLAY_TIME_RESET target:self selector:@selector(newGame) userInfo:nil repeats:NO];
}

-(void) gameCorrect {
    self.firstChoice.enabled = NO;
    self.secondChoice.enabled = NO;
    self.puzzleCount++;
    self.puzzleLabel.text = [self labelString:self.puzzleCount];
    self.gameState = STATE_CORRECT;
    self.firstChoice = nil;
    self.secondChoice = nil;
    [NSTimer scheduledTimerWithTimeInterval:DISPLAY_TIME_CORRECT target:self selector:@selector(newGame) userInfo:nil repeats:NO];
}

-(void) gameOver {
    NSString *s = (self.puzzleCount == 1 ? @"" : @"s");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Time's Up!" message:[NSString stringWithFormat:@"You completed %i puzzle%@ in %i seconds on %@ difficulty!", self.puzzleCount, s, TIMER_MULTIPLE * (self.difficulty + 1), [CQSharedManager getDifficultyName:self.game.difficulty]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    // optional - add more buttons:
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if([alertView.title isEqualToString:@"Time's Up!"]) {
        if (buttonIndex == alertView.cancelButtonIndex) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    else if ([alertView.title isEqualToString:@"Get Ready!"]) {
        if (buttonIndex == alertView.cancelButtonIndex) {
            [self newGame];
        }
    }
    else if ([alertView.title isEqualToString:@"Quitting!"]) {
        if (buttonIndex == 1) {
            self.isPaused = YES;
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void) motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    // Listens for the shake
    if(motion == UIEventSubtypeMotionShake)
    {
        self.isLabelDisplayed = !self.isLabelDisplayed;
        for(CQButton *b in self.buttonArray) {
            [b setIsLabelDisplayed:self.isLabelDisplayed];
        }
    }
}

-(void) setGameInProgress {
    self.firstChoice.enabled = YES;
    self.secondChoice.enabled = YES;
    self.firstChoice = nil;
    self.secondChoice = nil;
    self.gameState = STATE_IN_PROGRESS;
}


-(void) setGameState:(int)gameState {
    _gameState = gameState;
    if(gameState == STATE_IN_PROGRESS) {
        //[self.mainButton setTitle:@"?" forState:UIControlStateNormal];
        [self.mainButton setImage:[UIImage imageNamed:IMAGE_NAME_IN_PROGRESS] forState:UIControlStateNormal];
    }
    else if(gameState == STATE_RESET || gameState == STATE_WRONG) {
        [self.mainButton setImage:[UIImage imageNamed:IMAGE_NAME_WRONG] forState:UIControlStateNormal];
    }
    else if(gameState == STATE_CORRECT) {
        [self.mainButton setImage:[UIImage imageNamed:IMAGE_NAME_CORRECT] forState:UIControlStateNormal];
    }
}

-(void) clickBackButton:(UIButton *)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Quitting!" message:[NSString stringWithFormat:@"Are you sure that you want to quit?"] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    // optional - add more buttons:
    [alert show];
}

-(void) clickOptionButton:(UIButton *)sender {
    CQOptionViewController *optionViewController = [[CQOptionViewController alloc] init];
    [self.navigationController pushViewController:optionViewController animated:YES];
}


@end
